<?php
namespace Craft;

class ObscurePlugin extends BasePlugin {
  function getName() {
    return 'Obscure';
  }

  function getVersion() {
    return '1.1.0';
  }

  function getDeveloper() {
    return 'Crystal';
  }

  function getDeveloperUrl() {
    return 'http://wearecrystal.uk';
  }

  public function getDescription() {
    return Craft::t('Typekit and custom styles for redactor');
  }

  public function getSettingsHtml() {
    return craft()->templates->render('obscure/settings', array(
      'settings' => $this->getSettings()
    ));
  }

  protected function defineSettings() {
    return array(
      'cpcss' => array(AttributeType::String, 'required' => false, 'default' => ''),
      'tkid'  => array(AttributeType::String, 'required' => false, 'default' => ''),
      'async' => array(AttributeType::Bool, 'required' => false, 'default' => true),
    );
  }

  public function init() {
    if (craft()->request->isCpRequest()) {
      // include css file
      $settings = $this->getSettings();
      if (trim($settings->cpcss)) {
        $filepath = craft()->config->parseEnvironmentString($settings->cpcss);
        craft()->templates->includeCssFile($filepath);
      }

      // include typekit js
      if (trim($settings->tkid)) {
        craft()->templates->includeJsFile('https://use.typekit.net/'. $settings->tkid .'.js');
        craft()->templates->includeJs('try{Typekit.load({ async: true });}catch(e){}');
      }
    }
  }
}
